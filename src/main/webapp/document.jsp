<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html lang="fr" ng-app="MyApp">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>AnnotateMe !</title>

        <script type="text/javascript" src="js/jquery-2.0.3.js"></script>
        <script type="text/javascript" src="js/jquery-ui_1.10.4.min.js"></script>
        
        <script type="text/javascript" src="js/noty-2.2.2/packaged/jquery.noty.packaged.min.js"></script>

        <!--<script type="text/javascript" src="js/atmosphere-2.1.5.js"></script>
        <script type="text/javascript" src="js/jquery.atmosphere-2.1.5.js"></script>-->
        
        <script type="text/javascript" src="org/cometd.js"></script>
        <script type="text/javascript" src="jquery/jquery.cometd.js"></script>
        <script type="text/javascript" src="js/connectionCometD.js"></script>        

        <script src="js/angular/angular-1.2.13.min.js"></script>

        <link rel="stylesheet" type="text/css" href="js/angular/ui-layout-0.0.0/ui-layout.css"/>
        <script type="text/javascript" src="js/angular/ui-layout-0.0.0/ui-layout.min.js"></script>

        
        <link rel="stylesheet" type="text/css" href="css/layout-default-1.3.0.css"/>
        <script type="text/javascript" src="js/jquery.layout-1.3.0.min.js"></script>
  
        <link rel="stylesheet" type="text/css" href="css/dropit.css"/>
        <script type="text/javascript" src="js/dropit.js"></script>
 
        <link rel="stylesheet" href="css/google-plus-ui-buttons/css/css3-buttons.css" type="text/css" media="screen">
                
        
        <!--Must be loaded at the end to overwrite styles-->
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        
        <script type="text/javascript">
            var myAppModule = angular.module('MyApp', ['ui.layout']);
            
            function onWindowResize() {
                applicationController.loadPage(applicationController.currentPage);
            };
            
            
            window.onresize = function() {
                onWindowResize();
            };
            
            function submitDownloadRequest(format) {
                var options = {'documentId': applicationController.currentDocumentId
                               ,'format': format};
                $("#downloadOptionsInput").val(JSON.stringify(options));
                $("#downloadForm").submit();
            };
            
            
            var theLayout = null
            
            $(document).ready(function () {
                theLayout = $('body').layout(
                        { 
                            east: {
                                closable: false,
                                resizable: true,
                                size: "300"
                            }
                    });
                    
                $('.menu').dropit();    
                
                applicationController.alert = function(type/*success,error*/,text) {
                    var n = noty({layout:'topLeft', timeout: 5000,type: type,text: text});
                };
                
                applicationController.displayedCurrentPage = applicationController.currentPage;
                applicationController.scope.$watch('applicationController.currentPage', function(){
                    applicationController.displayedCurrentPage = applicationController.currentPage + 1;
                });
            });
            
            
        </script>


    </head>
    <body ng-controller="ApplicationController as appCtrl">
        
        
        
        
        <div id="downloadMenuDiv">
            <form id="downloadForm" action="AnnotateMeFileServlet" method="post" target="_blank">
                <input type="hidden" name="action" value="download">
                <input id="downloadOptionsInput" type="hidden" name="options">
                <ul class="menu">
                    <li>
                        <a class="btn" href="#">Download</a>
                        <ul>
                            <li><a href="javascript: submitDownloadRequest('pdf')">As PDF</a></li>
                            <li><a href="javascript: submitDownloadRequest('text')">As Text</a></li>
                        </ul>
                    </li>
                </ul>
            </form>
        </div>

        
               
        
        <div class="ui-layout-center">
            <div id="docView">
                <!--<img src="AnnotateMeFileServlet?id=123&page=2" width="100%"/>-->
                <img id="docViewImage" src="" style="z-index:0;position:absolute;top:0;left:0;">
                <canvas id="docViewCanvas" style="z-index:1;position:absolute;top:0;left:0;"></canvas>
            </div>
        </div>
           
       <div class="ui-layout-east">
           <div id="pagingToolbar">
               <a href="#" class="button" ng-click="appCtrl.previousPage()"><span class="icon icon8"></span></a>
               <input type="text" class="pageNumber" size="3" ng-model="appCtrl.displayedCurrentPage" onchange="var _v = this.value-1;applicationController.applyForUpdate(function(){applicationController.loadPage(_v);});"></input>
               <a href="#" class="button" ng-click="appCtrl.nextPage()"><span class="icon icon9"></span></a>
           </div>
           
            <div id="annotationView">
                <ul>
                    <li ng-repeat="annot in appCtrl.currentAnnotations track by $index">
                        <div class="annotationDiv" ng-class="annot._client.highlighted"
                             ng-mouseenter="appCtrl.annotationViewManager.highlightAnnotationView(annot.id)"
                             ng-mouseleave="appCtrl.annotationViewManager.unHighlightAnnotationView(annot.id)"
                             >
                            <!--<span>{{annot.id}}</span> <br/>-->
                            <textarea rows="1" cols="25" ng-model="annot.text" ng-change="applicationController.createUpdateAnnotationMessage(annot.id,annot.text,null,null,null,null)"></textarea>
                            
                            <a href="#" class="button buttonSmall" ng-click="appCtrl.deleteAnnotation(annot.id)">
                                <span class="icon icon186 buttonSmall"></span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </body>


    <script type="text/javascript" src="js/annotateme.js"></script>
    <script type="text/javascript" src="js/annotationView.js"></script>
    <script type="text/javascript">
        /*var _canvas = document.getElementById('docViewCanvas');
        var canvasAnnotationViewManager = new CanvasAnnotationViewManager(_canvas);

        canvasAnnotationViewManager.mouseDownHandler = onCreateNewAnnotation;

        applicationController.canvas = _canvas;
        applicationController.annotationViewManager = canvasAnnotationViewManager;*/
        
        /*var an1 = new AnnotationView(canvas);
        an1.x = 50;
        an1.width = 54;
        an1.height = 75;
        canvasAnnotationViewManager.addAnnotationView(an1*/
    </script>
    
    
</html>
