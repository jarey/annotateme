<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AnnotateMe !</title>

        <script type="text/javascript" src="js/jquery-2.0.3.js"></script>

        <!--<link rel="stylesheet" type="text/css" href="css/style.css"/>-->
        
        <script type="text/javascript" src="js/dropzone.js"></script>
        <script type="text/javascript">
            Dropzone.options.fileUploadForm = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            parallelUploads: 1,
            addRemoveLinks: true,
            clickable: true,
            maxFiles: 1,
            acceptedFiles: 'application/pdf',
            accept: function(file, done) {
                done();
              },
             init: function() {
                this.on("success", function(file,result) {
                    if("OK:" === result.substring(0,3)) {
                        var docId = result.substring(3);
                        window.location.href = "document.jsp?documentId=" + docId + "&page=0&";
                    }
                });
             },
             dictDefaultMessage: "Drag&Drop your PDF file here !"
            };
        </script>
        

        <style TYPE="text/css"> 
            <!-- 
            h1, h2 {
                text-align: center;
                color: #33334C;
            }
            #divFoot {
                width: 100%;
                height: 50px;  
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                text-align: center;
                background: #33334C;
                color: #FFFFFF;
            }
            #divFoot a {
                color: #FFFFFF;
            }
             
            #fileUploadDiv {
                border-radius: 3px;
                box-shadow: 0 0 50px rgba(0, 0, 0, 0.13);
                padding: 4px;
                cursor: pointer;
                width : 50%; 
                margin: auto;
                background: #33334C;
                color: #FFFFFF;
                height:150px;
                padding-top:75px; 
            }   
            #fileUploadDiv form {
                text-align: center;
                box-shadow: none;
            }
            .dz-message {
            }
            .dz-message span {
                font-size: 24px;
            }
            -->
        </style>
        


    </head>
    <body>

        <h1>AnnotateMe !</h1>
        <h2>A collaborative PDF document annotation editor</h2>
        
        <div id="fileUploadDiv">
            <form id="fileUploadForm" action="AnnotateMeFileServlet?action=upload" method="POST" enctype="multipart/form-data" class="dropzone">
                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
             </form>
        </div>
        

        <div id="divFoot">
            See project page: <a href="https://gitlab.com/medvedutka/annotateme/wikis/home">AnnotateMe!</a>
        </div>
    </body>
    
</html>
