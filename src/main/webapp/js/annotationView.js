function AnnotationView(_canvas) {
    canvas = _canvas;
}
AnnotationView.prototype = {
    id: new Date(),
    canvas: null,
    model: null,
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    highlighted: false,
    draw: function(ctx) {
        ctx.beginPath();
        ctx.rect(this.x, this.y, this.width, this.height);
        if(this.highlighted) {
            ctx.fillStyle = "rgba(150, 150, 150, 0.2)";;
        }
        else {
            ctx.fillStyle = "rgba(100, 100, 100, 0.5)";;
        }
        ctx.fill();
        if(this.highlighted) {
            ctx.lineWidth = 3;
            ctx.strokeStyle = 'red';
        }
        else {
            ctx.lineWidth = 2;
            ctx.strokeStyle = 'blue';
        }
        ctx.stroke();
    },
    isOver: function(px, py) {
        if (Math.abs(px - this.x) < 5) {
            if (Math.abs(py - this.y) < 5) {
                return 1;
            }
            else if (Math.abs(py - this.height - this.y) < 5) {
                return 7;
            }
            else if (py > this.y && py < (this.y + this.height)) {
                return 8;
            }
        }
        else if (Math.abs(px - this.width - this.x) < 5) {
            if (Math.abs(py - this.y) < 5) {
                return 3;
            }
            else if (Math.abs(py - +this.height - this.y) < 5) {
                return 5;
            }
            else if (py > this.y && py < (this.y + this.height)) {
                return 4;
            }
        }
        else if ((Math.abs(py - this.height - this.y) < 5) && (px > this.x) && (px < (this.x + this.width))) {
            return 6;
        }
        else if ((Math.abs(py - this.y) < 5) && (px > this.x) && (px < (this.x + this.width))) {
            return 2;
        }
        else if (px > this.x && py > this.y && px < (this.x + this.width) && py < (this.y + this.height)) {
            return 0;
        }
        return -1;
    }
};


function CanvasAnnotationViewManager(_canvas) {
    this.canvas = _canvas;
    this.context = this.canvas.getContext('2d');
    this.registerMouseListener();
}
CanvasAnnotationViewManager.prototype = {
    canvas: null,
    context: null,
    annotationsViews: [],
    currentSelection: {},
    currentMouseOverAnnotationId: null,
    addAnnotationView: function(av) {
        this.annotationsViews.push(av);
        this.draw();
    },
    removeAnnotationView: function(annotationId) {
        for(var i in this.annotationsViews) {
          if(this.annotationsViews[i].id === annotationId) {
              delete this.annotationsViews[i];
              return;
          }
      }
    },
    getAnnotationView: function(annotationId) {
        for(var i in this.annotationsViews) {
          if(this.annotationsViews[i].id === annotationId) {
              return this.annotationsViews[i];
          }
      }
    },
    clear : function() {
        this.annotationsViews = [];
        this.draw();
    },
    processMouseMove: function(evt) {
        var mousePos = this.getMousePos(evt);
        var px = mousePos.x;
        var py = mousePos.y;
        //console.log("move " + px + ","+ py);
        if (this.currentSelection.annotationView !== undefined) {
            var oldX = this.currentSelection.annotationView.x;
            var oldY = this.currentSelection.annotationView.y;
            switch (this.currentSelection.gripPosition) {
                case 0:
                    this.currentSelection.annotationView.x = px - this.currentSelection.gripX;
                    this.currentSelection.annotationView.y = py - this.currentSelection.gripY;
                    break;
                case 1:
                    this.currentSelection.annotationView.x = px - this.currentSelection.gripX;
                    this.currentSelection.annotationView.y = py - this.currentSelection.gripY;
                    this.currentSelection.annotationView.width = this.currentSelection.annotationView.width + (oldX - this.currentSelection.annotationView.x);
                    this.currentSelection.annotationView.height = this.currentSelection.annotationView.height + (oldY - this.currentSelection.annotationView.y);
                    break;
                case 2:
                    this.currentSelection.annotationView.y = py - this.currentSelection.gripY;
                    this.currentSelection.annotationView.height = this.currentSelection.annotationView.height + (oldY - this.currentSelection.annotationView.y);
                    break;
                case 3:
                    this.currentSelection.annotationView.width = px - this.currentSelection.annotationView.x;
                    this.currentSelection.annotationView.y = py - this.currentSelection.gripY;
                    this.currentSelection.annotationView.height = this.currentSelection.annotationView.height + (oldY - this.currentSelection.annotationView.y);
                    break;
                case 4:
                    this.currentSelection.annotationView.width = px - this.currentSelection.annotationView.x;
                    break;
                case 5:
                    this.currentSelection.annotationView.width = px - this.currentSelection.annotationView.x;
                    this.currentSelection.annotationView.height = py - this.currentSelection.annotationView.y;
                    break;
                case 6:
                    this.currentSelection.annotationView.height = py - this.currentSelection.annotationView.y;
                    break;
                case 7:
                    this.currentSelection.annotationView.x = px - this.currentSelection.gripX;
                    this.currentSelection.annotationView.width = this.currentSelection.annotationView.width + (oldX - this.currentSelection.annotationView.x);
                    this.currentSelection.annotationView.height = py - this.currentSelection.annotationView.y;
                    break;
                case 8:
                    this.currentSelection.annotationView.x = px - this.currentSelection.gripX;
                    this.currentSelection.annotationView.width = this.currentSelection.annotationView.width + (oldX - this.currentSelection.annotationView.x);
                    break;
            }

            this.draw();
        }

        var hitAView = false;
        for (var i in this.annotationsViews) {
            var view = this.annotationsViews[i];
            var hitResult = view.isOver(px, py);
            switch (hitResult) {
                case 0:
                    this.canvas.style.cursor = "move";
                    break;
                case 1:
                    this.canvas.style.cursor = "nw-resize";
                    break;
                case 2:
                    this.canvas.style.cursor = "n-resize";
                    break;
                case 3:
                    this.canvas.style.cursor = "ne-resize";
                    break;
                case 4:
                    this.canvas.style.cursor = "e-resize";
                    break;
                case 5:
                    this.canvas.style.cursor = "se-resize";
                    break;
                case 6:
                    this.canvas.style.cursor = "s-resize";
                    break;
                case 7:
                    this.canvas.style.cursor = "sw-resize";
                    break;
                case 8:
                    this.canvas.style.cursor = "w-resize";
                    break;
                default:
                    this.canvas.style.cursor = "auto";
                    break;
            }

            if (hitResult >= 0) {
                if(this.currentMouseOverAnnotationId !== view.id) {
                    if(this.currentMouseOverAnnotationId !== null) {
                        this.annotationViewMouseLeave(this.currentMouseOverAnnotationId);
                    }
                    this.currentMouseOverAnnotationId = view.id;
                    this.annotationViewMouseEnter(view.id);
                }
                hitAView = true;
                break;
            }
        }
        if(!hitAView) {
            if(this.currentMouseOverAnnotationId !== null) {
                this.annotationViewMouseLeave(this.currentMouseOverAnnotationId);
            }
            this.currentMouseOverAnnotationId = null;
        }
    },
    mouseDownHandler: function(x,y){},
    annotationViewChangeHandler : function(annotationView){},
    annotationViewMouseEnter: function(id) {},
    annotationViewMouseLeave: function(id) {},
    highlightAnnotationView: function(id) {
        var view = this.getAnnotationView(id);
        if(view !== undefined) {
            view.highlighted = true;
        }
        this.draw();
    },
    unHighlightAnnotationView: function(id) {
        var view = this.getAnnotationView(id);
        if(view !== undefined) {
            view.highlighted = false;
        }        
        this.draw();
    },
    processMouseDown: function(evt) {
        var mousePos = this.getMousePos(evt);
        var px = mousePos.x;
        var py = mousePos.y;
        //console.log("down " + px + ","+ py);
        for (var i in this.annotationsViews) {
            var view = this.annotationsViews[i];
            var hitResult = view.isOver(px, py);
            if (hitResult >= 0) {
                this.currentSelection.annotationView = view;
                this.currentSelection.gripX = px - view.x;
                this.currentSelection.gripY = py - view.y;
                this.currentSelection.gripPosition = hitResult;
                return;
            }
        }

        if (this.currentSelection.annotationView === undefined) {
            this.mouseDownHandler(px,py);
            /*var anv = new AnnotationView(this.canvas);
            anv.x = px;
            anv.y = py;
            anv.width = 75;
            anv.height = 30;
            this.addAnnotationView(anv);
            this.draw();*/
        }

        this.currentSelection = {};
    },
    processMouseUp: function(evt) {
        var mousePos = this.getMousePos(evt);
        var px = mousePos.x;
        var py = mousePos.y;
        if(this.currentSelection.annotationView !== undefined) {
            this.annotationViewChangeHandler(this.currentSelection.annotationView);
        }
        this.currentSelection = {};
    },
    draw: function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        for (var i in this.annotationsViews) {
            var view = this.annotationsViews[i];
            view.draw(this.context);
        }
        console.log("CanvasAnnotationViewManager: draw");
    },
    getMousePos: function(evt) {
        var rect = this.canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    },
    registerMouseListener: function() {
        var _this = this;
        this.canvas.addEventListener('mousemove', function(evt) {
            _this.processMouseMove(evt);
        }, false);
        this.canvas.addEventListener('mousedown', function(evt) {
            _this.processMouseDown(evt);
        }, false);
        this.canvas.addEventListener('mouseup', function(evt) {
            _this.processMouseUp(evt);
        }, false);
    }
};

