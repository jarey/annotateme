
function ConnectionCometD() {
    _self = this; //crap but needed
    $.cometd.websocketEnabled = false;
    //_self.cometdURL = 'http://localhost:8080/AnnotateMe/cometd';
    
    _self.onMessage = function(message) {};
    _self.onOpen = function(response) {};
    
    _self.initialise();
    _self.connect();
}

ConnectionCometD.prototype = {
    _self: null,
    connected: false,
    //cometdURL: null,
    _connected: false,
    _disconnecting: false,
    _documentSubscription: null,
    _serviceRequestSubscription: null,
    _docId: null,
    _uuid: new Date().toTimeString(),


    statusNotification: function(level /*success,error*/,msg) {
      console.log(""+level +": " + msg)  ;
    },
    initialise: function() {
        this._docId = GetURLParameter("documentId");
        $.cometd.addListener('/meta/handshake', this._metaHandshake);
        $.cometd.addListener('/meta/connect', this._metaConnect);
    },

    getUUID: function() {
        return this._uuid;
    },
    connect: function() {
        this._disconnecting = false;
        $.cometd.configure({
            url: './cometd', //'http://localhost:8080/AnnotateMe/cometd'
            logLevel: 'debug'
        });
        $.cometd.handshake();        
    },
    disconnect: function() {
        $.cometd.batch(function()
        {
            //$.cometd.publish('/chat/demo', {data: 'leaving'});
             if (this._documentSubscription) {
                $.cometd.unsubscribe(this._documentSubscription);
            }
            this._documentSubscription = null;
        });
        $.cometd.disconnect();
        this._disconnecting = true;
    },
    send: function(msgObject) {
        $.cometd.publish('/service/request/'+ this._docId, JSON.stringify(msgObject)); // TODO: we may and able to not encode as String
    },
    receive: function(message){
        //console.log(message.data);
        _self.onMessage(message.data);
    },

    _connectionInitialized: function() {
        this._uuid = $.cometd.getClientId();
        // first time connection for this client, so subscribe tell everybody
        var subscribtionUrl = '/AnnotatedMeChannel/document/' + this._docId;
        this._documentSubscription = $.cometd.subscribe(subscribtionUrl,_self.receive);
        subscribtionUrl = '/service/request/' + this._docId;
        this._serviceRequestSubscription = $.cometd.subscribe(subscribtionUrl, _self.receive);
        _self.onOpen("Connection Initialized");
    },
    _connectionEstablished: function() {
        // connection establish (maybe not for first time), so just tell local user and update membership
        //_self.receive({data: {user: 'system',chat: 'Connection to Server Opened'}});
        //$.cometd.publish('/service/members', {user: _username,room: '/chat/demo'});
    },
    _connectionBroken: function() {
        //_self.receive({data: {user: 'system',chat: 'Connection to Server Broken'}});
    },
    _connectionClosed: function() {
        //_self.receive({data: {user: 'system',chat: 'Connection to Server Closed'}});
    },
    _metaConnect: function(message) {
        if (this._disconnecting) {
            _self._connected = false;
            _self._connectionClosed();
        }
        else {
            var wasConnected = this._connected;
            _self._connected = message.successful === true;
            if (!wasConnected && this._connected) {
                _self._connectionEstablished();
            }
            else if (wasConnected && !this._connected) {
                _self._connectionBroken();
            }
        }
    },
    _metaHandshake: function(message) {
        if (message.successful) {
            _self._connectionInitialized();
        }
    }
    
    

};





//Utils
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) 
        {
            return sParameterName[1];
        }
    }
}
