var applicationController = null;


function ApplicationController($scope) {    
    var _this = this;
    _this.scope = $scope;
    _this.currentUser = {username:"thecat"};
    _this.currentDocumentId = GetURLParameter("documentId");
    _this.currentPage = (GetURLParameter("page") === undefined) ? 0 : parseInt(GetURLParameter("page"));
    _this.nbTotalPage = -1;
    _this.currentAnnotations = {};
    
    _this.image = document.getElementById("docViewImage");
    _this.divContainer =  document.getElementById("docView");
    _this.canvas = document.getElementById('docViewCanvas');
    _this.annotationViewManager = new CanvasAnnotationViewManager(_this.canvas);
    _this.annotationViewManager.mouseDownHandler = onCreateNewAnnotation;
    _this.annotationViewManager.annotationViewChangeHandler = viewMoved;
    _this.annotationViewManager.annotationViewMouseEnter =  function(id) {
        $scope.$apply(function() {
            var annot = _this.getAnnotation(id);
            if(annot !== undefined) {
                setClientProperty(annot,"highlighted", "annot-highlighted");
            }
        });
    };
    _this.annotationViewManager.annotationViewMouseLeave= function(id) {
        $scope.$apply(function() {
            var annot = _this.getAnnotation(id);
            if(annot !== undefined) {
                setClientProperty(annot,"highlighted", "");
            }
        });
    };
    applicationController = _this;
    $scope.applicationController = _this;
    
    _this.applyForUpdate = function(fn) { // Apply 'fn' in AngularJs scope
          $scope.$apply(fn);
    };
    _this.getDocumentURL = function(docId, pageNumber) { 
        return "AnnotateMeFileServlet?id=" + docId + "&page=" + pageNumber;
    };
    _this.displayDocument = function (docId, pageNumber) {
        _this.image.src = "resources/ajax-loader.gif";
        _this.image.width = 32;
        _this.image.height = 32;
        var img = new Image();
        img.onload = function(){
            var ratio = img.height / img.width;
            var w = _this.divContainer.clientWidth;
            var h = w * ratio;//_this.divContainer.clientHeight;
            _this.image.width = w;
            _this.image.height = h;
            _this.image.src = _this.getDocumentURL(docId,pageNumber);
            _this.canvas.setAttribute("width", w);
            _this.canvas.setAttribute("height", h);
            _this.listAnnotationMessage(docId,pageNumber);        
            _this.currentPage = pageNumber;
        };
        img.src = _this.getDocumentURL(docId,pageNumber);
        console.log("DisplayDocument: loading");
    };
    _this.nextPage = function() {
        _this.loadPage(_this.currentPage + 1);
    };
    _this.previousPage = function() {
        var pageNumber = _this.currentPage;
        if(pageNumber > 0) {
           pageNumber--;
        }
        _this.loadPage(pageNumber);
    };
    _this.loadPage = function(pageNumber) {
        removeAllAnnotation();
        _this.clearAnnotations();
        _this.displayDocument(_this.currentDocumentId,pageNumber);
    };
    _this.clearAnnotations = function() {
        _this.currentAnnotations = {};
    };
    _this.addAnnotation = function(annotation) {
        _this.currentAnnotations[annotation.id] = annotation;
    };
    _this.deleteAnnotation = function(annotationId) {
        _this.deleteAnnotationMessage(annotationId);
    };    
    _this.getAnnotation = function(id){
        return _this.currentAnnotations[id];
    };
    _this.alert = function(type/*success,error*/,text) { //this may be redefined to provide better user experience alert report
        console.log(type + ": " + text);
    };
    
    var connection = new ConnectionCometD();
    connection.onOpen = function(response) {
        console.log('onOpen: ', response);
        _this.loadPage(_this.currentPage);
        _this.alert("success","You are now connected.");
    };
    connection.onMessage = function(message) {
        var json = null;
        try {
            json = JSON.parse(message);
        } 
        catch (e) {
            console.log('This doesn\'t look like a valid JSON: ', message);
            return;
        }

        var msgHandler = null;
        switch(json['@class']) {
            case ".Message":
                msgHandler = _this.onSimpleMessage;
                break;
            case ".ListAnnotation":
                msgHandler = _this.onListAnnotationMessage_response;
                break;
            case ".CreateAnnotation":
                msgHandler = _this.onCreateAnnotation_response;
                break;
            case ".NewAnnotation":
                msgHandler = _this.onNewAnnotation;
                break;
            case ".DeleteAnnotation":
                msgHandler = _this.onDeleteAnnotation;
                break;
            case ".UpdateAnnotation":
                msgHandler = _this.onUpdateAnnotation;
                break;
            default:
                console.log("Message not understood: " + message);
        }
        $scope.$apply(function(){
            //console.log("msg in> " + message);
            msgHandler(json);
        });
    };

    
    ////////////////
    // Messages
    _this.newMessage = function(msgText) {
        var msg = {"@class":".Message",author: _this.currentUser.userName
            ,message:msgText
        };
        _this.sendMessage(msg);
    };
    _this.createAnnotationMessage = function(text,x,y,w,h) {
        var msg = {"@class":".CreateAnnotation",author: _this.currentUser.userName
            ,documentId: _this.currentDocumentId, documentPage: _this.currentPage
            ,text:text
            ,x:x,y:y,w:w,h:h
        };
        _this.sendMessage(msg);
    };
    _this.deleteAnnotationMessage = function(annotationId) {
        var msg = {"@class":".DeleteAnnotation",author: _this.currentUser.userName
            ,documentId: _this.currentDocumentId
            ,annotationId:annotationId
        };
        _this.sendMessage(msg);
    };    
    _this.createUpdateAnnotationMessage = function(annotationId,text,x,y,w,h) {
        var msg = {"@class":".UpdateAnnotation",author: _this.currentUser.userName
            ,documentId: _this.currentDocumentId
            ,annotationId:annotationId
            ,text:text
            ,x:x,y:y,w:w,h:h
        };
        _this.sendMessage(msg);
    };    
    _this.listAnnotationMessage = function(documentId,page) {
        var msg = {"@class":".ListAnnotation",author: _this.currentUser.userName
            ,documentId:documentId
            ,page:page
        };
        _this.sendMessage(msg);
    };
    _this.sendMessage = function(jsonMsg) {
        jsonMsg.clientUUID = connection.getUUID();
        var strMsg = JSON.stringify(jsonMsg);
        connection.send(jsonMsg);
    };
    
    ////////////////
    // OnMessages
    _this.onSimpleMessage = function(msg) {
        //nothing to do!
    };
    _this.onListAnnotationMessage_response = function(msg) {
        console.log("onListAnnotationMessage");
        removeAllAnnotation();
        _this.clearAnnotations();
        for(var i in msg.annotations) {
            _this.addAnnotation(msg.annotations[i]); 
            displayAnnotation(msg.annotations[i]);
         //   console.log(JSON.stringify(msg.annotations[i]));
        }
        _this.annotationViewManager.draw();
    };
    _this.onNewAnnotation = function(msg) {
        if((msg.documentId === _this.currentDocumentId) && (msg.annotation.page === _this.currentPage)) {
            if(_this.getAnnotation(msg.annotation.id) === undefined) {
                _this.addAnnotation(msg.annotation);
                displayAnnotation(msg.annotation);
            }
        }
    };
    _this.onDeleteAnnotation = function(msg) {
        if(msg.documentId === _this.currentDocumentId) {
            if(_this.getAnnotation(msg.annotationId) !== undefined) {
                delete _this.currentAnnotations[msg.annotationId];
                _this.annotationViewManager.removeAnnotationView(msg.annotationId);
                _this.annotationViewManager.draw();
            }
        }
    };
    _this.onCreateAnnotation_response = function(msg) {
        if(_this.getAnnotation(msg.result.id) === undefined) {
            _this.addAnnotation(msg.result);
            displayAnnotation(_this.getAnnotation(msg.result.id));
        }
        //TODO: must select it
    };
   _this.onUpdateAnnotation = function(updateAnnotationMsg) {
      if(connection.getUUID() !== updateAnnotationMsg.clientUUID) {
            var annotation = _this.getAnnotation(updateAnnotationMsg.annotationId);
            if(annotation !== undefined) {
                updateObjectINN(updateAnnotationMsg,annotation,['text']);

                var movedOrScaled = updateObjectINN(updateAnnotationMsg,annotation,['x','y',['w','width'],['h','height']]);;
                if(movedOrScaled) {
                     updateAnnotationView(annotation);
                }
           }
           else {
               console.error("Annotation unknown: " + updateAnnotationMsg.annotationId + ". Can't update it.");
           }
       }
    };
          

    _this.loadDocument = function(documentId) {
        _this.listAnnotationMessage(documentId,1);
    };


    _this.loadPage(_this.currentPage);
}


/**
 * updateObject IfNotNull
 * Update 'fields' field of object 'to' from object 'from' only if they are not null
 * @param {type} from
 * @param {type} to
 * @param {type} fields
 * @returns true if something changed
 */
function updateObjectINN(from,to,fields) {
    var result = false;
    for(var i in fields) {
        var field= fields[i];
        var value = from[(field instanceof Array) ? field[0] : field];
        if(value !== null && value !== undefined) {
            to[(field instanceof Array) ? field[1] : field] = value;
            result = true;
        }
    }
    return result;
}

function setClientProperty(object,property,value) {
    if(object._client === undefined) {
        object._client = {};
    }
    object._client[property] = value;
}




function local_createNewAnnotation(text,x,y,w,h) {
    applicationController.createAnnotationMessage(text,x,y,w,h);
}
function local_annotationMoved(annotationId,x,y,w,h) {
    applicationController.createUpdateAnnotationMessage(annotationId,null,x,y,w,h);
}





/////////////////////////
//Annotation views management
function onCreateNewAnnotation(x,y) {
    /*var anv = new AnnotationView(this.canvas);
    anv.x = x;
    anv.y = y;
    anv.width = 75;
    anv.height = 30;
    applicationController.annotationViewManager.addAnnotationView(anv);*/
    var ax = x / applicationController.canvas.clientWidth;
    var ay = y / applicationController.canvas.clientHeight;
    var aw = 75 / applicationController.canvas.clientWidth;;
    var ah = 30 / applicationController.canvas.clientHeight;
    local_createNewAnnotation("new annotation", ax ,ay,aw,ah);
    //applicationController.annotationViewManager.draw();
}



function updateAnnotationView(annotation) {
    var view = applicationController.annotationViewManager.getAnnotationView(annotation.id);
    updateAnnotationViewFromAnnotation(annotation,view);
    applicationController.annotationViewManager.draw();
}
function removeAnnotation(annotation) {
    applicationController.annotationViewManager.removeAnnotationView(annotation.id);
    applicationController.annotationViewManager.draw();
}
function removeAllAnnotation() {
    applicationController.annotationViewManager.clear();
}
function displayAnnotation(annotation) {
    var anv = new AnnotationView(this.canvas);
    anv.id = annotation.id;
    updateAnnotationViewFromAnnotation(annotation,anv);
    applicationController.annotationViewManager.addAnnotationView(anv);
}
function viewMoved(view) {
    applicationController.scope.$apply(function(){
        var annotation = applicationController.getAnnotation(view.id); 
        updateAnnotationFromAnnotationView(view,annotation);
        local_annotationMoved(annotation.id,annotation.x,annotation.y,annotation.width,annotation.height);
    });
}
function updateAnnotationFromAnnotationView(view, annotation) {   
    annotation.x = view.x / applicationController.canvas.clientWidth;
    annotation.y = view.y / applicationController.canvas.clientHeight;
    annotation.width = view.width / applicationController.canvas.clientWidth;
    annotation.height = view.height / applicationController.canvas.clientHeight;  
}
function updateAnnotationViewFromAnnotation(annotation,view) {
    view.x = annotation.x * applicationController.canvas.clientWidth;
    view.y = annotation.y * applicationController.canvas.clientHeight;
    view.width = annotation.width * applicationController.canvas.clientWidth;
    view.height = annotation.height * applicationController.canvas.clientHeight;  
}



//Utils
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) 
        {
            return sParameterName[1];
        }
    }
}
