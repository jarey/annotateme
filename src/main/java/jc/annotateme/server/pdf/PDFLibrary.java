/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server.pdf;

import java.awt.image.BufferedImage;
import java.io.File;

/**
 *
 * @author thecat
 */
public interface PDFLibrary {
    int getNumberOfPage(File pdfFile) throws Exception;
    BufferedImage renderAsImage(File pdfFile,int pageNumber) throws Exception;
}
