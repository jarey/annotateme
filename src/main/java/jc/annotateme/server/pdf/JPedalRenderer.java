/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server.pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.jpedal.PdfDecoder;
import org.jpedal.constants.JPedalSettings;
import org.jpedal.exception.PdfException;
import org.jpedal.fonts.FontMappings;
import org.jpedal.objects.PdfPageData;

import org.jpedal.PDFtoImageConvertor;

/**
 *
 * @author thecat
 */
public class JPedalRenderer implements PDFLibrary {

    @Override
    public int getNumberOfPage(File pdfFile) throws Exception {
        PdfDecoder decode_pdf = new PdfDecoder(true);
        /**set mappings for non-embedded fonts to use*/
        FontMappings.setFontReplacements();

        try {
            decode_pdf.openPdfFile(pdfFile.getAbsolutePath()); //file
            //decode_pdf.openPdfFile("C:/myPDF.pdf", "password"); //encrypted file
            //decode_pdf.openPdfArray(bytes); //bytes is byte[] array with PDF
            //decode_pdf.openPdfFileFromURL("http://www.mysite.com/myPDF.pdf",false);

            /**get page 1 as an image*/
            //page range if you want to extract all pages with a loop
            return decode_pdf.getPageCount();
        } catch (PdfException e) {
            throw e;
        }
        finally {
            decode_pdf.closePdfFile();
        }        
    }

    @Override
    public BufferedImage renderAsImage(File pdfFile, int pageNumber) throws Exception {
        PdfDecoder decode_pdf = new PdfDecoder(true);

        /**set mappings for non-embedded fonts to use*/
        FontMappings.setFontReplacements();
        
        try {
            decode_pdf.openPdfFile(pdfFile.getAbsolutePath()); //file
            //decode_pdf.openPdfFile("C:/myPDF.pdf", "password"); //encrypted file
            //decode_pdf.openPdfArray(bytes); //bytes is byte[] array with PDF
            //decode_pdf.openPdfFileFromURL("http://www.mysite.com/myPDF.pdf",false);

            decode_pdf.scaling = 2;
            
            return decode_pdf.getPageAsImage(pageNumber + 1);
        } catch (PdfException e) {
            throw e;
        }
        finally {
            decode_pdf.closePdfFile();
        }
    }
    
    
    
    
    
}
