/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server.storage;

import com.db4o.Db4oEmbedded;
import com.db4o.EmbeddedObjectContainer;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.constraints.UniqueFieldValueConstraint;
import com.db4o.ta.TransparentActivationSupport;
import com.db4o.ta.TransparentPersistenceSupport;
import java.io.File;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import jc.annotateme.model.Annotation;
import jc.annotateme.model.Document;
import jc.annotateme.server.Config;

public class Db4oServletListener implements ServletContextListener/*, ServletRequestListener*/ {
    public static final String KEY_DB4O_FILE_NAME = "database-file-name";

    public static final String KEY_DB4O_SERVER = "db4oServer";
    public static final String KEY_DB4O_SESSION = "db4oSession";

    public static EmbeddedObjectContainer DB_SERVER;
    
    //#example: db4o-instance for the web-application
    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        File dbFile = new File(Config.Get().getDBPath(),context.getInitParameter(KEY_DB4O_FILE_NAME));
        String filePath = dbFile.getAbsolutePath();
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        //config.common().add(new TransparentActivationSupport());    
        //config.common().add(new TransparentPersistenceSupport());
        config.common().activationDepth(Integer.MAX_VALUE);
        config.common().updateDepth(Integer.MAX_VALUE);
        
        config.common().add(new UniqueFieldValueConstraint(Document.class, "id"));
        config.common().add(new UniqueFieldValueConstraint(Annotation.class, "id"));
	config.common().objectClass(Document.class).objectField("id").indexed(true);
        config.common().objectClass(Annotation.class).objectField("id").indexed(true);
	config.common().objectClass(Document.class).objectField("annotations").cascadeOnDelete(true);
        
        EmbeddedObjectContainer rootContainer = Db4oEmbedded.openFile(config,filePath);
        DB_SERVER = rootContainer;
        context.setAttribute(KEY_DB4O_SERVER, rootContainer);
        
        //debugInit(rootContainer);
        //Document doc =debugLoadDocument(rootContainer);
        
        context.log("db4o startup on " + filePath);
    }
    
    private void debugInit(EmbeddedObjectContainer dbServer) {
        ObjectContainer dbSession = dbServer.ext().openSession();        
        try {
            Document doc = new Document();
            doc.setId("123");
            doc.setDateCreation("20121112");
            doc.setOriginalFileName("123.pdf");
            doc.setTitle("Le PDF 123");
            dbSession.store(doc);
            dbSession.commit();
        }
        catch(Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Error in debugInit", e);
        }
        finally {
            if(dbSession != null) {
                dbSession.close();
            }
        }
    }
    
    private Document debugLoadDocument(EmbeddedObjectContainer dbServer) {
        String documentId = "123";
        ObjectContainer dbSession = dbServer.ext().openSession();        
        try {
            Document document = new Document();
            document.setId(documentId);
            ObjectSet<Document> documents  = dbSession.queryByExample(document);
            if(documents.size() != 1) {
                java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE,"Cannot retrieve doc 123 !");
                return null;
            }
            return documents.next();
        }
        catch(Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Error in debugInit", e);
        }
        finally {
            if(dbSession != null) {
                dbSession.close();
            }
        }
        return null;
    }
    
    
    
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        ObjectContainer rootContainer = (ObjectContainer) context.getAttribute(KEY_DB4O_SERVER);
        context.removeAttribute(KEY_DB4O_SERVER);
        DB_SERVER = null;
        close(rootContainer);
        context.log("db4o shutdown");
    }
    // #end example
    
    
    /*
    @Override
    public void requestInitialized(ServletRequestEvent requestEvent) {
        EmbeddedObjectContainer rootContainer = (EmbeddedObjectContainer) requestEvent
                .getServletContext().getAttribute(Db4oServletListener.KEY_DB4O_SERVER);
    
        ObjectContainer session = rootContainer.ext().openSession();
        requestEvent.getServletRequest().setAttribute(KEY_DB4O_SESSION, session);
    }
    
    
    @Override
    public void requestDestroyed(ServletRequestEvent requestEvent) {
        ObjectContainer session = (ObjectContainer) requestEvent
                .getServletRequest().getAttribute(KEY_DB4O_SESSION);
    
        close(session);
    }
    */
    
    
    private void close(ObjectContainer container) {
        if (container != null) {
            container.close();
        }
        container = null;
    }
}
