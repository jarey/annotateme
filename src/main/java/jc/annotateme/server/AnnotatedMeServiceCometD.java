/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server;

    
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import jc.annotateme.model.message.ErrorMessage;
import jc.annotateme.model.message.Message;

import org.cometd.annotation.Configure;
import org.cometd.annotation.Listener;
import org.cometd.annotation.Service;
import org.cometd.annotation.Session;
import org.cometd.bayeux.client.ClientSessionChannel;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.ConfigurableServerChannel;
import org.cometd.bayeux.server.ServerMessage;
import org.cometd.bayeux.server.ServerSession;
import org.cometd.server.authorizer.GrantAuthorizer;


@Service("AnnotatedMeService")
public class AnnotatedMeServiceCometD implements AnnotateMeChannelService {
    
    private static final Logger LOGGER = java.util.logging.Logger.getLogger("AnnotatedMeServiceCometD");
    
   //private final ConcurrentMap<String, Map<String, String>> _members = new ConcurrentHashMap<>();
    @Inject
    private BayeuxServer _bayeux;
    @Session
    private ServerSession _session;

    
    private final JacksonEncoder messageToJSON = new JacksonEncoder();
    private final JacksonDecoder jsonToMessage = new JacksonDecoder();
  

    @Configure ("/AannotatedMeChannel/document/**")
    protected void configurePrivateChat(ConfigurableServerChannel channel) {
        channel.setPersistent(false);
        channel.setLazy(false);
        channel.addAuthorizer(GrantAuthorizer.GRANT_ALL);
    }

    @Listener("/AnnotatedMeChannel/document/**")
    public void privateChat(ServerSession client, ServerMessage message) {
        /*ServerMessage.Mutable response = _bayeux.newMessage();
        response.setChannel(message.getChannel());
        response.setId("" + System.nanoTime());
        response.setData("Response: " + message.getJSON());
        client.deliver(_session, response);*/
    }



    @Configure ("/service/request/**")
    protected void configureServiceRequest(ConfigurableServerChannel channel) {
        channel.setPersistent(true);
        channel.addAuthorizer(GrantAuthorizer.GRANT_ALL);
    }

    @Listener("/service/request/**")
    public void serviceRequest(ServerSession client, ServerMessage serverMessage) {
        try {
            Message message = jsonToMessage.decode((String)serverMessage.getData());
            AnnotateMeMsgProcessor msgProcessor = new AnnotateMeMsgProcessor(this);
            Message response = msgProcessor.process(message);
            if(response != null) {
                ServerMessage.Mutable serverResponse = _bayeux.newMessage();
                serverResponse.setId("" + System.nanoTime());
                serverResponse.setChannel(serverMessage.getChannel());
                serverResponse.setData(messageToJSON.encode(message));
                client.deliver(_session, serverResponse);
            }
        }
        catch(Exception e) {
            LOGGER.log(Level.SEVERE, "Error on 'serviceRequest':", e);
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.setMessage(e.getMessage());
            ServerMessage.Mutable serverResponse = _bayeux.newMessage();
            serverResponse.setId("" + System.nanoTime());
            serverResponse.setChannel(serverMessage.getChannel());
            serverResponse.setData(messageToJSON.encode(errorMessage));
            client.deliver(_session, serverResponse);
        }
    }
    
    
    
    @Override
    public void broadcast(String documentId, Message message) {
        ClientSessionChannel channel = _session.getLocalSession().getChannel("/AnnotatedMeChannel/document/"+documentId);
        channel.publish(messageToJSON.encode(message));
    }
 

}