/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.annotateme.server;

import com.db4o.EmbeddedObjectContainer;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.rmi.ServerException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jc.annotateme.model.Annotation;
import jc.annotateme.model.Document;
import jc.annotateme.server.pdf.PDFBoxRenderer;
import jc.annotateme.server.pdf.PDFLibrary;
import jc.annotateme.server.storage.Db4oServletListener;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;



public class AnnotateMeFileServlet extends HttpServlet {

    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(AnnotateMeFileServlet.class.getSimpleName());

    //private ServletFileUpload uploader = null;
    
    
    @Override
    public void init() throws ServletException{
        /*DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        fileFactory.setRepository(Config.Get().getTmpPath());
        this.uploader = new ServletFileUpload(fileFactory);*/
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        if("upload".equals(action)) {
            uploadDocument(request,response);
        }
        else if("download".equals(action)) {
            downloadDocument(request,response);
        }
        else {
            sendDocumentPage(request, response);
        }
    }
    
    
    private void sendDocumentPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idParam = request.getParameter("id");
        String pageParam = request.getParameter("page");
        String rendererParam = request.getParameter("renderer");

        //TODO validate params.
        String id = idParam;
        int page = Integer.parseInt(pageParam);

        try {
            sendDocumentPage(id, page,rendererParam, request, response);
        } catch (Exception e) {
            throw new ServletException(e);
        }  
    }
    
    

    private void sendDocumentPage(String id, int page,String rendererParam, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PDFLibrary pdfLibrary = null;
        if(rendererParam == null || rendererParam.isEmpty()) {
            String pdfRenderer = "jc.annotateme.server.pdf." + Config.Get().getPDFLibrary();
            pdfLibrary = (PDFLibrary)Class.forName(pdfRenderer).newInstance();
        }
        else {
            String pdfRenderer = "jc.annotateme.server.pdf." + rendererParam;
            pdfLibrary = (PDFLibrary)Class.forName(pdfRenderer).newInstance();
        }
        
        File docFile = documentFilePathFromID(id);
        BufferedImage image = pdfLibrary.renderAsImage(docFile, page);
        response.setContentType("image/jpeg");
        OutputStream outputStream = response.getOutputStream();
        ImageIO.write(image, "jpeg", outputStream);
        outputStream.close();
    }

    private File documentFilePathFromID(String id) {
        //TODO
        Config config = Config.Get();
        File docFile = new File(config.getDocumentPath(), id + ".pdf");
        return docFile;
    }

    
    
    private void uploadDocument(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(!ServletFileUpload.isMultipartContent(request)){
            throw new ServletException("Content type is not multipart/form-data");
        }

        response.setContentType("text/html;charset=UTF-8");

        OutputStream out = null;
        InputStream filecontent = null;
        final PrintWriter writer = response.getWriter();

        EmbeddedObjectContainer dbServer = Db4oServletListener.DB_SERVER;
        ObjectContainer dbSession = dbServer.ext().openSession();     
        try {
            String documentId = UUID.randomUUID().toString();

            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setRepository(Config.Get().getTmpPath());
            ServletFileUpload uploader = new ServletFileUpload(factory);
            
            List<FileItem> fileItemsList = uploader.parseRequest(request);
            Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
            if(fileItemsIterator.hasNext()){
                FileItem fileItem = fileItemsIterator.next();
                //System.out.println("FieldName="+fileItem.getFieldName());
                //System.out.println("FileName="+fileItem.getName());
                //System.out.println("ContentType="+fileItem.getContentType());
                //System.out.println("Size in bytes="+fileItem.getSize());

                String fileName = fileItem.getName();
                File outputFile = new File(Config.Get().getDocumentPath(), documentId + ".pdf");
                out = new FileOutputStream(outputFile);
                fileItem.write(outputFile);
                LOGGER.log(Level.INFO, "File{0}being uploaded to {1}",new Object[]{fileName, outputFile.getAbsolutePath()});

                Document document = new Document();
                document.setId(documentId);
                document.setOriginalFileName(fileName);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                document.setDateCreation(sdf.format(new Date()));
                document.setTitle(fileName);
                dbSession.store(document);
                dbSession.commit();
                writer.println("OK:" + documentId);
            }
        } 
        catch (Exception fne) {
            writer.println("ERROR: You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent "
                    + "location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",new Object[]{fne.getMessage()});
        } 
        finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
            if (writer != null) {
                writer.close();
            }
            if(dbSession != null) {
                dbSession.close();
            }
        }
    }

    
    
    private void downloadDocument(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String optionsParameter = request.getParameter("options");
        DownloadOptions options = null;
        try {
             ObjectMapper mapper = new ObjectMapper();
             options = mapper.readValue(optionsParameter, DownloadOptions.class);
        }
        catch(Exception e) {
            throw new ServerException("Cannot read download options from request", e);
        }
        
        OutputStream outputStream = response.getOutputStream();
        EmbeddedObjectContainer dbServer = Db4oServletListener.DB_SERVER;
        ObjectContainer dbSession = dbServer.ext().openSession();     
        try {
            Document document = loadDocument(dbSession,options.documentId);
    
            if(options.isExportAsPdf()) {
                response.setContentType("application/pdf");
                response.addHeader("Content-Disposition", "attachment; filename=" + document.getOriginalFileName()); //TODO: must escape caracters

                File docFile = documentFilePathFromID(document.getId());
                List<Annotation> annotations = document.getAnnotations();
                if(annotations == null) {
                    annotations = new LinkedList<Annotation>();
                }
                PDFAnnotationCreator.AddAnnotationsTo(docFile, annotations, outputStream);
            }
            else if(options.isExportAsText()) {
                TxtAnnotationExport txtAnnotationExport = new TxtAnnotationExport();
                String annotationsTxt = txtAnnotationExport.export(document);
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/plain; charset=UTF-8");
                response.addHeader("Content-Disposition", "attachment; filename=" + replaceExtension(document.getOriginalFileName(),".txt")); //TODO: must escape caracters
                
                PrintWriter out = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF-8"), true);
                try {
                    out.write(annotationsTxt);
                    out.flush();
                }
                catch(Exception e) {
                    throw e;
                }
                finally {
                    out.close();
                }
            }
            else {
                throw new Exception("Download format unknown: '" + options.format + "'.");
            }
        } 
        catch(Exception fne) {
            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",new Object[]{fne.getMessage()});
        } 
        finally {
            if(dbSession != null) {
                dbSession.close();
            }
            if(outputStream != null) {
                outputStream.close();
            }
        }
        
        
    }
    
    
    private String replaceExtension(String fileName, String newExtension) {
        int index = fileName.lastIndexOf('.');
        if(index == -1) {
            return fileName + newExtension;
        }
        return fileName.substring(0, index) + newExtension;
    }
    
    
    private Document loadDocument(ObjectContainer dbSession, String documentId) throws Exception {
        Document document = new Document();
        document.setId(documentId);
        ObjectSet<Document> documents  = dbSession.queryByExample(document);
        if(documents.size() != 1) {
            throw new Exception("No document found with ID: " + documentId);
        }
        return documents.next();
    }
    
    
    
    
    public static class DownloadOptions {
        public DownloadOptions() {
        }
        
        private String documentId;
        private String format;

        public String getDocumentId() {
            return documentId;
        }

        public void setDocumentId(String documentId) {
            this.documentId = documentId;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }
        
        
        
        public boolean isExportAsPdf() {
            return "pdf".equals(format);
        }
        public boolean isExportAsText() {
            return "text".equals(format);
        }
    }
    
    
    
    /*
    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }*/

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "AnnotateMeFileServlet servlet";
    }// </editor-fold>

}
