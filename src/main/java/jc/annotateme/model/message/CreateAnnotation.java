/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.model.message;

import jc.annotateme.model.Annotation;



public class CreateAnnotation extends Message {
    private Float x;
    private Float y;
    private Float w;
    private Float h;
    private String text;

    private String documentId;
    private Integer documentPage;
    
    private Annotation result;
    
    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getW() {
        return w;
    }

    public void setW(Float w) {
        this.w = w;
    }

    public Float getH() {
        return h;
    }

    public void setH(Float h) {
        this.h = h;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    
    public Annotation getResult() {
        return result;
    }

    public void setResult(Annotation result) {
        this.result = result;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Integer getDocumentPage() {
        return documentPage;
    }

    public void setDocumentPage(Integer documentPage) {
        this.documentPage = documentPage;
    }
    
    
    
    
}
