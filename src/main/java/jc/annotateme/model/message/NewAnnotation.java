/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.annotateme.model.message;

import jc.annotateme.model.Annotation;



public class NewAnnotation extends Message {

    private Annotation annotation;
    private String documentId;

    public String getDocumentId() {
        return documentId;
    }
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    public Annotation getAnnotation() {
        return annotation;
    }
    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }

}
